from django.contrib import admin

from .models import Question, Choice


#=============== Version 4: Tabular Display================
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,  {'fields': ['question_text']}),
        ('Date information',  {'fields': ['pub_date'], 
        'classes': ['collapse']}),        
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date'] # For filtering
    search_fields = ['question_text'] # For searching

admin.site.register(Question, QuestionAdmin)

#=================== Version 3 ========================
""" class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 3

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,  {'fields': ['question_text']}),
        ('Date information',  {'fields': ['pub_date'], 
        'classes': ['collapse']}),        
    ]
    inlines = [ChoiceInline]

admin.site.register(Question, QuestionAdmin) """

# Version 2
""" class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,  {'fields': ['question_text']}),
        ('Date information',  {'fields': ['pub_date']}),
    ]

admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice) """



